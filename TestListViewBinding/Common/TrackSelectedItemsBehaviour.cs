﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xaml.Interactivity;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace TestListViewBinding.Common
{
	public class TrackSelectedItemsBehaviour:DependencyObject, IBehavior
	{
		public DependencyObject AssociatedObject
		{
			get { return AssociatedListView; }
		}

		private ListView AssociatedListView { get; set; }



		public void Attach(DependencyObject associatedObject)
		{
			this.AssociatedListView = associatedObject as ListView;
			this.AttachEvents();
			
		}
		public void Detach()
		{
			this.DetachEvents();
		}


		

		
		private void AttachEvents()
		{
			this.AssociatedListView.SelectionChanged += AssociatedListView_SelectionChanged;
		}

		private void DetachEvents()
		{
			this.AssociatedListView.SelectionChanged -= AssociatedListView_SelectionChanged;
		}


		public static DependencyProperty SelectedCountProperty = DependencyProperty.RegisterAttached("SelectedCount", typeof(int), typeof(TrackSelectedItemsBehaviour), new PropertyMetadata(0));
		public int SelectedCount
		{
			get { return (int)GetValue(SelectedCountProperty); }
			set { SetValue(SelectedCountProperty, value); }
		}


		void AssociatedListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var diff = e.AddedItems.Count - e.RemovedItems.Count;
			SelectedCount += diff;
		}



	}

	

}
