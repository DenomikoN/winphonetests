﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace TestListViewBinding.Common
{
	public class TestConverter:IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			Debug.WriteLine("Convert:{0}", value);
			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			Debug.WriteLine("Convert Back:{0}", value);
			return value;
		}
	}
}
