﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TestListViewBinding.Common
{
	public class TestCommand:ICommand
	{


		public bool CanExecute(object parameter)
		{
			return true;
		}

		public event EventHandler CanExecuteChanged;

		public void Execute(object parameter)
		{
			 
			Debug.WriteLine(parameter);
		}
	}

	public class RelayCommand : ICommand
	{
		public Action<object> ExecuteAction { get; private set; }
		public Func<object,bool> CanExecuteFunc { get; private set; }

		public RelayCommand(Action<object> executeAction, Func<object,bool> canExecuteFunc = null)
		{
			this.ExecuteAction = executeAction;
			this.CanExecuteFunc = canExecuteFunc ?? ((a) => true);
		}



		public bool CanExecute(object parameter)
		{
			return CanExecuteFunc(parameter);
		}

		public event EventHandler CanExecuteChanged = delegate { };

		public void Execute(object parameter)
		{
			ExecuteAction(parameter);
		}


		public void RaiseCanExecuteChanged(EventArgs args)
		{
			CanExecuteChanged(this,args ?? new EventArgs());
		}



	}


}
