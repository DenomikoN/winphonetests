﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace TestListViewBinding.Common
{
	public class EnumValuesConverter : IValueConverter
	{
		public class EnumValue
		{
			public object Key { get; set; }
			public String Desc { get; set; }
		}


		public object Convert(object value, Type targetType, object parameter, string language)
		{
			if(value != null){
				var type = value.GetType();
				var ret = Enum.GetValues(type);
				return ret;
			}
			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			throw new NotSupportedException();
		}
	}
}
