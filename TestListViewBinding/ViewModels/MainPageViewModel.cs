﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestListViewBinding.Common;

namespace TestListViewBinding.ViewModels
{
	public class MainPageViewModel
	{
		private int idx = 0;
		public ObservableCollection<User> Users { get; set; }


		ICommand _testCommand = null;
		public ICommand TestCommand
		{
			get
			{
				return _testCommand ?? (_testCommand = new TestCommand());
			}
		}

		ICommand _testCommand2 = null;
		public ICommand TestCommand2
		{
			get
			{
				return _testCommand2 ?? (_testCommand2 = new TestCommand());
			}
		}


		ICommand _addUserCommand = null;
		public ICommand AddUserCommand
		{
			get
			{
				return _addUserCommand ?? (_addUserCommand = new RelayCommand(arg => this.AddRandomUser(), arg => Users.Count < 10));
			}
		}



		public MainPageViewModel()
		{
			this.Users = new ObservableCollection<User>();
		}

		public void FillMockData()
		{
			Users.Add(new User { Id = idx++, FirstName = "Denis", LastName = "Antonenko" });
			Users.Add(new User { Id = idx++, FirstName = "Vasiliy", LastName = "Pupkin" });
			Users.Add(new User { Id = idx++, FirstName = "Sergey", LastName = "Sergeev" });
		}


		static List<String> FirstNames = new List<string>() { "Fiona","Gabrielle","Grace","Hannah","Heather","Irene"
			,"Jan","Jane","Jasmine","Jennifer","Jessica","Joan","Joanne","Julia","Karen","Katherine","Kimberly","Kylie"};
		static List<String> LastNames = new List<string>() {"Black","Blake","Bond","Bower","Brown","Buckland","Burgess","Butler","Cameron","Campbell","Carr","Chapman",
			"Churchill","Clark","Clarkson","Coleman","Cornish" };

		public void AddRandomUser()
		{
			Random rnd = new Random();
			Users.Add(new User { Id = idx++, FirstName = FirstNames[rnd.Next(0, FirstNames.Count - 1)], LastName = LastNames[rnd.Next(0, LastNames.Count - 1)] });
		}
		


	}
	public class User
	{
		public int Id {get;set;}
		public string FirstName { get; set; }
		public string LastName { get; set; }

	}

}
